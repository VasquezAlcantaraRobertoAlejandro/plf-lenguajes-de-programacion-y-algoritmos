# Mapa conceptual 1
## Programando ordenadores en los 80 y ahora. ¿Que ha cambiado? (2016)
```plantuml
@startmindmap
*[#FF4500] Programando ordenadores en los 80 y ahora.
	*[#FFA500] Antes
		* Se necesita conocer la arquitectura
			* Conocer la máquina a detalle
		* Solo se podía usar ensamblador
			* Mayor control
			* Código más eficiente
			* Porque se tenían pocos recursos
			* Prioriza la eficiencia
	*[#FFA500] Diferencias a resaltar
		* Antes había limitantes en cuento a recursos
		* Ahora el programador se centra en la complejidad del programa
		* Ahora hay mucha potencia en cuento ordenadores
	*[#FFA500] Ahora
		* Desventajas
			* Sobre carga de capas
			* Uso excesivo de librerías
			* El software ocupa más recursos
		* Lenguajes ocupados
			* Lenguajes de alto nivel
				* Software más complejo
				* El código se puede ejecutar en cualquier máquina
				* Más pesados
				* Prima la claridad al momento de programar
				* Mayor comodidad
		* No se necesita saber la arquitectura
@endmindmap
```

# Mapa conceptual 2
## Hª de los algoritmos y de los lenguajes de programación (2010)

```plantuml
@startmindmap
*[#FF4500] Historia de los algoritmos \ny de los lenguajes de programación(2010)
	*[#FFA500] Algoritmos
		* Historia
			* Se emplean desde hace 3000 años
				* En el siglo 17 aparecen \nlas primeras calculadoras
		* Es un procedimiento
		* Debe ser
			* Ordenada
			* Finita
			* No dar lugar a ambigüedades
	*[#FFA500] Lenguajes de Programación
		* Herramientas
			* Hardware
			* Sistema Operativo
			* Usaurio
			* Software
		* Definición
			* Lenguaje diseñado para describir acciones \nque deben ser ejecutadas por una máquina
		* Historia
			* Código binario (Máquina)
				* Lenguaje ensamblador
					* Lenguajes de alto nivel
		* Paradigmas
			* Métodos para la contrucción de programas
				* Paradigma imperativo (1960)
				* Paradigma Funcional (1960)
				* Paradigma orientado a objetos (1968)
				* Paradigma Lógico (1971)
@endmindmap
```
# Mapa conceptual 3
## Tendencias actuales en los lenguajes de Programación y Sistemas Informáticos. La evolución de los lenguajes y paradigmas de programación (2005)

```plantuml
@startmindmap
*[#FF4500] Tendencias actuales en los lenguajes \nde Programación y Sistemas Informáticos
	*[#FFA500] Paradigmas
		*[#F08080] Comunes
			* Programación esctruturada
				* Concepción modular al programar
				* Lenguajes
					* Basic
					* Java
					* Pascal
					* C
			* Programación funcional
				* Se apoya en las matemáticas
				* Usa la recursividad
				* Lenguajes
					* ML
					* HOPE
					* Haskell
			* Programación lógica
				* Usa expresiones lógicas
				* No usa letras ni números
				* Lenguajes
					* Prolog
					* Mercury
					* CLP
				* Usa recursividad
				* Modelizar un problema
		*[#F08080] Actuales
			* Programación concurrente
				* Soluciona que múltiples usuarios accedan al sistema
				* Lenguajes
					* Haskell
				* Implementa politicas
				* Tiene que tener en cuenta a los usuarios
			* Programación distribuida
				* Surge para la comunicación con múltiples ordenadores
				* Lenguajes
					* Ada
					* Erlang
					* Limbo
			* Programación orientada a objetos
				* Más abstracto
				* Lenguajes
					* Java
				* Uso de objetos independientes
			* Programación basada en componentes
				* Reutiliza código
				* Lenguajes
					* Pascal
					* Java Scrip
					* Cecil
				* Nivel mayor de abstracción
@endmindmap
```
